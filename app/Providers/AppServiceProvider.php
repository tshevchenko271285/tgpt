<?php

namespace App\Providers;

use App\Services\Contracts\GPTServiceInterface;
use App\Services\Contracts\TGBotServiceInterface;
use App\Services\GPTService;
use App\Services\TGBotService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * All the container bindings that should be registered.
     *
     * @var array
     */
    public $bindings = [
        TGBotServiceInterface::class => TGBotService::class,
        GPTServiceInterface::class => GPTService::class,
    ];

    /**
     * Register any application services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        //
    }
}
