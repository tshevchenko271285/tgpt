<?php

namespace App\Console\Commands;

use App\Services\Contracts\TGBotServiceInterface;
use Illuminate\Console\Command;

class Install extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Installs TG Bot';

    /**
     * @param TGBotServiceInterface $TGBotService
     */
    public function __construct(
        protected TGBotServiceInterface $TGBotService
    ) {
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        if ($this->TGBotService->install()) {
            $this->info('Install done!');
        } else {
            $this->info('Something went wrong!');
        }
    }
}
