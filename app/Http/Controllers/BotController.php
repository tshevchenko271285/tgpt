<?php

namespace App\Http\Controllers;

use App\Services\Contracts\TGBotServiceInterface;
use Illuminate\Http\Request;
use Telegram\Bot\Exceptions\TelegramSDKException;

class BotController extends Controller
{
    /**
     * @param TGBotServiceInterface $TGBotService
     */
    public function __construct(
        protected TGBotServiceInterface $TGBotService
    ) {}

    /**
     * @param Request $request
     * @return void
     * @throws TelegramSDKException
     */
    public function callback(Request $request): void
    {
        $this->TGBotService->requestHandler($request->all());
    }
}
