<?php

namespace App\Http\Telegram;

use App\Services\Contracts\GPTServiceInterface;
use Telegram\Bot\Commands\Command;

class ClearContextCommand extends Command
{
    /**
     * @param GPTServiceInterface $GPTService
     */
    public function __construct(
        protected GPTServiceInterface $GPTService,
    ) {}

    protected string $name = 'clear';
    protected string $description = 'Clears context the chat.';

    public function handle()
    {
        try {
            $chatId = $this->getTelegram()->getWebhookUpdate()->getMessage()->getChat()->getId();
            $message = $this->GPTService->clearContext($chatId) ? 'Context cleared.' : 'Context is empty.';
        } catch (\Throwable $e) {
            $message = $e->getMessage();
        }

        $this->replyWithMessage([
            'text' => $message,
        ]);
    }
}
