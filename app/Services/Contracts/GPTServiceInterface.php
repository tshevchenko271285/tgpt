<?php

namespace App\Services\Contracts;

interface GPTServiceInterface
{
    /**
     * Sends question to the GPT chat.
     *
     * @param int $chatID
     * @param string $message
     * @return string
     */
    public function sendQuestion(int $chatID, string $message): string;

    /**
     * Clears the chat context.
     *
     * @param int $chatID
     * @return bool
     */
    public function clearContext(int $chatID): bool;
}
