<?php

namespace App\Services\Contracts;

use Telegram\Bot\Exceptions\TelegramSDKException;

interface TGBotServiceInterface
{
    /**
     * Sets up the bot
     *
     * @return bool
     * @throws TelegramSDKException
     */
    public function install(): bool;

    /**
     * Processes the main request from the chat.
     *
     * @param array $data
     * @return void
     * @throws TelegramSDKException
     */
    public function requestHandler(array $data): void;
}
