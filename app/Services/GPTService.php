<?php

namespace App\Services;

use App\Services\Contracts\GPTServiceInterface;
use Illuminate\Support\Facades\Cache;
use OpenAI;
use OpenAI\Client as OpenAIClient;

class GPTService implements GPTServiceInterface
{
    const CACHE_PREFIX = 'gpt_';

    protected OpenAIClient $openAIClient;

    public function __construct()
    {
        $this->openAIClient = OpenAI::factory()
            ->withApiKey(config('openai.api_key'))
            ->withOrganization(config('openai.organization'))
            ->make();
    }

    /**
     * Sends question to the GPT chat.
     *
     * @param int $chatID
     * @param string $message
     * @return string
     * @throws \Exception
     */
    public function sendQuestion(int $chatID, string $message): string
    {
        $cacheKey = $this->cacheKey($chatID);
        $choices = Cache::get($cacheKey, []);
        $choices[] = $this->prepareContext(['role' => 'user', 'content' => $message]);

        $result = $this->openAIClient->chat()->create([
            'model' => config('openai.model_gpt'),
            'messages' => $choices,
        ]);

        $gptAnswer = $this->prepareContext($result['choices'][0]['message']);
        $choices[] = $gptAnswer;

        Cache::put($cacheKey, $choices, now()->addMinutes(10));

        return strip_tags($gptAnswer['content']);
    }

    /**
     * Clears the chat context.
     *
     * @param int $chatID
     * @return bool
     */
    public function clearContext(int $chatID): bool
    {
        return Cache::delete($this->cacheKey($chatID));
    }

    /**
     * @param array $params
     * @return array
     * @throws \Exception
     */
    protected function prepareContext(array $params): array
    {
        if (empty($params['content'])) {
            throw new \Exception('Your question is empty.');
        }
        return [
            'role' => !empty($params['role']) ? $params['role'] : 'user',
            'content' => !empty($params['content']) ? $params['content'] : '',
        ];
    }

    /**
     * @param int $chatID
     * @return string
     */
    protected function cacheKey(int $chatID): string
    {
        return self::CACHE_PREFIX . $chatID;
    }
}
