<?php

namespace App\Services;

use App\Services\Contracts\GPTServiceInterface;
use App\Services\Contracts\TGBotServiceInterface;
use Illuminate\Support\Facades\Log;
use Telegram\Bot\Api;
use Telegram\Bot\Exceptions\TelegramSDKException;
use Telegram\Bot\Laravel\Facades\Telegram;

class TGBotService implements TGBotServiceInterface
{
    /**
     * @param Api $telegramClient
     * @param GPTServiceInterface $GPTService
     */
    public function __construct(
        protected Api $telegramClient,
        protected GPTServiceInterface $GPTService,
    ) {}

    /**
     * Sets up the bot
     *
     * @return bool
     * @throws TelegramSDKException
     */
    public function install(): bool
    {
        return $this->telegramClient->setWebhook(['url' => config('telegram.bots.mybot.webhook_url')]);
    }

    /**
     * Processes the main request from the chat.
     *
     * @param array $data
     * @return void
     * @throws TelegramSDKException
     */
    public function requestHandler(array $data): void
    {
        try {
            if (Telegram::commandsHandler(true)?->getMessage()?->hasCommand()) {
                return;
            }

            if (!empty($data['message']) && !empty($data['message']['text'])) {
                $chatID = $data['message']['chat']['id'];
                $message = $data['message']['text'];

                $this->textHandler($chatID, $message);
            } else {
                throw new \Exception('Does not support message type!');
            }
        } catch (\Throwable $e) {
            $chatID = $this->telegramClient->getWebhookUpdate()->getMessage()->getChat()->getId();

            $this->telegramClient->sendMessage([
                'chat_id' => $chatID,
                'text' => $e->getMessage(),
                'parse_mode' => 'HTML',
            ]);

            Log::info(__METHOD__, [$e]);
        }
    }

    /**
     * Processes a text request from the chat.
     *
     * @param int $chatID
     * @param string $message
     * @return void
     * @throws TelegramSDKException
     */
    protected function textHandler(int $chatID, string $message): void
    {
        $waitMessage = $this->telegramClient->sendMessage([
            'chat_id' => $chatID,
            'text' => "\u{1F914}",
            'parse_mode' => 'HTML',
        ]);

        $responseText = $this->GPTService->sendQuestion($chatID, $message);

        $this->telegramClient->deleteMessage([
            'chat_id' => $chatID,
            'message_id' => $waitMessage->getMessageId(),
        ]);

        $this->telegramClient->sendMessage([
            'chat_id' => $chatID,
            'text' => $responseText,
            'parse_mode' => 'HTML',
        ]);
    }
}
